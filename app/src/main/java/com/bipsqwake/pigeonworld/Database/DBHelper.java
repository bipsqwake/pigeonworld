package com.bipsqwake.pigeonworld.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bipsqwake.pigeonworld.framework.FileIO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by bipsq on 10.03.2017.
 */
public class DBHelper extends SQLiteOpenHelper {
    final static int DB_VERSION = 10;
    private FileIO fileIO;

    public DBHelper(Context context, FileIO fileIO) {
        super(context, "GameDB", null, DB_VERSION);
        this.fileIO = fileIO;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileIO.readAsset("create.sql")));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                db.execSQL(line);
            }
        } catch (IOException e) {

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + "landscapes");
        db.execSQL("drop table if exists " + "neighborhs");
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileIO.readAsset("create.sql")));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                db.execSQL(line);
            }
        } catch (IOException e) {

        }
    }
}
