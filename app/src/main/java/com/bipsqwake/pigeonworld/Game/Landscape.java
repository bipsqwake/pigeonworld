package com.bipsqwake.pigeonworld.Game;

import com.bipsqwake.pigeonworld.framework.Game;
import com.bipsqwake.pigeonworld.framework.gl.FileTexture;
import com.bipsqwake.pigeonworld.framework.gl.Texture;
import com.bipsqwake.pigeonworld.framework.gl.TextureRegion;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;
import com.bipsqwake.pigeonworld.framework.math.Vector2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 20.02.2017.
 */
public class Landscape {
    public TextureRegion textureRegion;
    List<Surface> ground = new LinkedList<>();
    List<Surface> platforms = new LinkedList<>();
    List<Door> doors = new LinkedList<>();
    public float rightBorder;
    public float leftborder;
    public boolean hasRoof = false;
    public float roof;
    public Landscape right = null;
    public Landscape left = null;
    public Landscape up = null;
    public int id;

    Landscape(GLGame game, String surfaceFile, String textureFile, int right, int left, int up, int id) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(game.getFileIO().readAsset(surfaceFile)));
            int count = Integer.parseInt(reader.readLine());
            for (int i = 0; i < count; i++) {
                StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
                ground.add(new Surface(Integer.parseInt(tokenizer.nextToken()), Integer.parseInt(tokenizer.nextToken()),
                        Integer.parseInt(tokenizer.nextToken())));
            }
            count = Integer.parseInt(reader.readLine());
            for (int i = 0; i < count; i++) {
                StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
                platforms.add(new Surface(Integer.parseInt(tokenizer.nextToken()), Integer.parseInt(tokenizer.nextToken()),
                        Integer.parseInt(tokenizer.nextToken())));
            }
            count = Integer.parseInt(reader.readLine());
            for (int i = 0; i < count; i++) {
                StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
                Surface surface = new Surface(Integer.parseInt(tokenizer.nextToken()),
                                            Integer.parseInt(tokenizer.nextToken()),
                                            Integer.parseInt(tokenizer.nextToken()));
                doors.add(new Door(surface, Integer.parseInt(tokenizer.nextToken()),
                        new Vector2(Integer.parseInt(tokenizer.nextToken()), Integer.parseInt(tokenizer.nextToken()))));
            }
            Texture texture = new FileTexture(game, textureFile);
            texture.load();
            texture.bind();
            texture.setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
            textureRegion = new TextureRegion(texture, 0, 0, 1080, 1080);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        rightBorder = right;
        leftborder = left;
        roof = up;
        this.id = id;
    }

    public int getY(float x) {
        for (Surface a: ground) {
            if (a.from <= x && x <= a.to) {
                return a.height;
            }
        }
        return 0;
    }

    public List<Surface> getPlatforms(float x) {
        List<Surface> result = new LinkedList<>();
        for (Surface a: platforms) {
            if (a.from <= x && x <= a.to) {
                result.add(a);
            }
        }
        for (Surface a: ground) {
            if (a.from <= x && x <= a.to) {
                result.add(a);
                break;
            }
        }
        return result;
    }

    public void addDoor(Door door) {
        doors.add(door);
    }

    public Door getDoor(float x, float y) {
        for (Door a: doors) {
            if (a.toStand.from <= x && x <= a.toStand.to && a.toStand.height == y) {
                return a;
            }
        }
        return null;
    }
}
