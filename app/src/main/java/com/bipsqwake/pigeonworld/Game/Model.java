package com.bipsqwake.pigeonworld.Game;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bipsqwake.pigeonworld.Database.DBHelper;
import com.bipsqwake.pigeonworld.framework.gl.Animation;
import com.bipsqwake.pigeonworld.framework.gl.FileTexture;
import com.bipsqwake.pigeonworld.framework.gl.Texture;
import com.bipsqwake.pigeonworld.framework.gl.TextureRegion;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;
import com.bipsqwake.pigeonworld.framework.impl.Utils;
import com.bipsqwake.pigeonworld.framework.math.Vector2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 20.02.2017.
 */
public class Model {
    public Pigeon pigeon;
    public Map<Integer, Landscape> landscapes = new HashMap<>();
    GLGame game;
    DBHelper helper;

    public Model(GLGame game) {
        this.game = game;
        createPigeon();
        createLandscape();
        helper = new DBHelper(game, game.getFileIO());
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery("select * from savedposition", null);
        c.moveToFirst();
        int land = c.getColumnIndex("land");
        int x = c.getColumnIndex("x");
        int y = c.getColumnIndex("y");
        pigeon.setLandscape(landscapes.get(c.getInt(land)));
        pigeon.position.set(c.getInt(x), c.getInt(y));
        c.close();

    }

    private void createPigeon() {
        pigeon = new Pigeon(landscapes);
        Texture pigeonAtlas = new FileTexture(game, "pigeon.png");
        pigeonAtlas.load();
        pigeonAtlas.bind();
        pigeonAtlas.setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
        pigeon.setTexture(pigeonAtlas);
        Animation idle = new Animation(0.25f, new TextureRegion(pigeonAtlas, 0, 0, 128, 128),
                new TextureRegion(pigeonAtlas, 128, 0, 128, 128),
                new TextureRegion(pigeonAtlas, 256, 0, 128, 128),
                new TextureRegion(pigeonAtlas, 384, 0, 128, 128));
        Animation fly = new Animation(0.1f, new TextureRegion(pigeonAtlas, 0, 128, 128, 128),
                new TextureRegion(pigeonAtlas, 128, 128, 128, 128),
                new TextureRegion(pigeonAtlas, 256, 128, 128, 128),
                new TextureRegion(pigeonAtlas, 384, 128, 128, 128));
        Animation move = new Animation(0.09f, new TextureRegion(pigeonAtlas, 0, 256, 128, 128),
                new TextureRegion(pigeonAtlas, 128, 256, 128, 128),
                new TextureRegion(pigeonAtlas, 256, 256, 128, 128),
                new TextureRegion(pigeonAtlas, 384, 256, 128, 128));
        pigeon.setIdleAnimation(idle);
        pigeon.setFlyAnimation(fly);
        pigeon.setMoveAnimation(move);
    }

    private void createLandscape() {
        String json = "";
        try {
            json = Utils.fromStream(game.getFileIO().readAsset("landscapes.json"));
            JSONObject file = new JSONObject(json);
            JSONArray lands = file.getJSONArray("landscapes");
            for (int i = 0; i < lands.length(); i++) {
                JSONObject land = lands.getJSONObject(i);
                int id = land.getInt("id");
                String textureFile = land.getString("texture");
                String platformFile = land.getString("platforms");
                int right = land.getInt("rightBorder");
                int left = land.getInt("leftBorder");
                int up = land.getInt("upBorder");
                landscapes.put(id, new Landscape(game, platformFile, textureFile, right, left, up, id));
            }
            JSONArray links = file.getJSONArray("links");
            for (int i = 0; i < links.length(); i++ ) {
                JSONObject link = links.getJSONObject(i);
                int id = link.getInt("from");
                if (link.has("right")) {
                    int right = link.getInt("right");
                    landscapes.get(id).right = landscapes.get(right);
                }
                if (link.has("left")) {
                    int left = link.getInt("left");
                    landscapes.get(id).left = landscapes.get(left);
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    public void reload() {
        for (Map.Entry<Integer, Landscape> landscape :landscapes.entrySet())
            landscape.getValue().textureRegion.texture.reload();
        pigeon.texture.reload();
    }

    public void onPause() {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete("savedposition", null, null);
        cv.put("x", pigeon.position.x);
        cv.put("y", pigeon.position.y);
        cv.put("land", pigeon.landscape.id);
        db.insert("savedposition", null, cv);
    }
}
