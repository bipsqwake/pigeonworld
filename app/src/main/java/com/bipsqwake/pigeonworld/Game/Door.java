package com.bipsqwake.pigeonworld.Game;

import com.bipsqwake.pigeonworld.framework.math.Vector2;

/**
 * Created by bipsq on 03.03.2017.
 */
public class Door {
    Surface toStand;
    int toGo;
    Vector2 newCoord;

    public Door (Surface toStand, int toGo, Vector2 newCoord) {
        this.toGo = toGo;
        this.toStand = toStand;
        this.newCoord = newCoord;
    }
}
