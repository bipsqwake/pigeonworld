package com.bipsqwake.pigeonworld.Game;

import android.util.Log;

import com.bipsqwake.pigeonworld.framework.Input;
import com.bipsqwake.pigeonworld.framework.gl.FileTexture;
import com.bipsqwake.pigeonworld.framework.gl.SpriteBatcher;
import com.bipsqwake.pigeonworld.framework.gl.Texture;
import com.bipsqwake.pigeonworld.framework.gl.TextureRegion;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;
import com.bipsqwake.pigeonworld.framework.impl.GLGraphics;
import com.bipsqwake.pigeonworld.framework.math.Vector2;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 20.02.2017.
 */
public class StandartControl {
    private final static int PAD_START_DIST = 70;
    private final static int PAD_END_DIST = 150;
    Model model;
    GLGame game;
    Map<Integer, Vector2> touches = new HashMap<>();
    boolean padActive = false;
    int padTouchIndex = 0;
    Vector2 padCurrentPos = null;
    TextureRegion padDown;
    TextureRegion padUp;
    TextureRegion enterButton;
    SpriteBatcher batcher;
    boolean enterButtonActive = false;

    public StandartControl(Model model, GLGame game) {
        this.game = game;
        this.model = model;
        Texture texture = new FileTexture(game, "pad.png");
        texture.load();
        texture.bind();
        texture.setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
        padDown = new TextureRegion(texture, 0, 0, 128, 128);
        padUp = new TextureRegion(texture, 128, 0, 128, 128);
        enterButton = new TextureRegion(texture, 256, 0, 256, 128);
        batcher = new SpriteBatcher(game.getGlGraphics(), 3);
    }

    public void update(float deltaTime) {
        List<Input.TouchEvent> events = game.getInput().getTouchEvents();
        Input.TouchEvent event;
        int len = events.size();
        for (int i = 0; i < len; i++) {
            event = events.get(i);
            if (event.type == Input.TouchEvent.TOUCH_DOWN) {
                touches.put(event.pointer, new Vector2(event.x, event.y));
            }
            if (event.type == Input.TouchEvent.TOUCH_DRAGGED && touches.containsKey(event.pointer)) {
                if (!padActive && touches.get(event.pointer).dist(event.x, event.y) > PAD_START_DIST) {
                    padActive = true;
                    padTouchIndex = event.pointer;
                    padCurrentPos = new Vector2(event.x, event.y);
                } else {
                    padCurrentPos = new Vector2(event.x, event.y);
                }
            }
            if (event.type == Input.TouchEvent.TOUCH_UP) {
                touches.remove(event.pointer);
                if (padActive && padTouchIndex == event.pointer) {
                    padActive = false;
                } else {
                }
            }
        }
        if (padActive) {
            Vector2 dir = new Vector2(padCurrentPos).sub(touches.get(padTouchIndex));
            model.pigeon.setMove(Vector2.angle(new Vector2(1, 0), dir));
        } else {
            model.pigeon.drop();
        }
        model.pigeon.update(deltaTime);
        if (model.pigeon.isOnDoor()) {
            enterButtonActive = true;
        } else {
            enterButtonActive = false;
        }
    }

    public void present() {
        if (padActive) {
            GLGraphics graphics = game.getGlGraphics();
            GL10 gl = graphics.getGL();
            gl.glMatrixMode(GL10.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glOrthof(0, 720, 0, 1280, 1, -1);
            batcher.beginBatch(padDown.texture);
            batcher.drawSprite(touches.get(padTouchIndex).x, touches.get(padTouchIndex).y, 300, 300, padDown, SpriteBatcher.NORMAL);
            Vector2 padPos = new Vector2(padCurrentPos).sub(touches.get(padTouchIndex));
            if (padPos.len() <= PAD_END_DIST) {
                batcher.drawSprite(padCurrentPos.x, padCurrentPos.y, 100, 100, padUp, SpriteBatcher.NORMAL);
            } else {
                padPos.normalize().mul(PAD_END_DIST).add(touches.get(padTouchIndex));
                batcher.drawSprite(padPos.x, padPos.y, 100, 100, padUp, SpriteBatcher.NORMAL);
            }
            batcher.endBatch();
        }
    }

    public void reload() {
        padUp.texture.reload();
    }

    private boolean inBounds(Input.TouchEvent event, int x, int y, int width,
                             int height) {
        return event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1;
    }

}
