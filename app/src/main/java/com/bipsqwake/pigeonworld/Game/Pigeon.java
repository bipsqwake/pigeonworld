package com.bipsqwake.pigeonworld.Game;

import com.bipsqwake.pigeonworld.framework.gl.Animation;
import com.bipsqwake.pigeonworld.framework.gl.Texture;
import com.bipsqwake.pigeonworld.framework.gl.TextureRegion;
import com.bipsqwake.pigeonworld.framework.math.Vector2;

import java.util.List;
import java.util.Map;

/**
 * Created by bipsq on 19.02.2017.
 */
public class Pigeon {
    private static final float FLY_SPEED = 4.0f;
    private static final float WALK_SPEED = 2.0f;
    private static final float FALL_SPEED = 7.5f;
    enum State{
        Idle,
        Falling,
        Flying,
        Moving
    }
    enum Direction {
        Right,
        Left
    }
    public Texture texture;
    Animation idleAnimation;
    Animation flyAnimation;
    Animation moveAnimation;

    public Landscape landscape;
    Map<Integer, Landscape> landscapes;
    float startTime = 0.0f;

    public Vector2 position = new Vector2(360, 50);

    State state = State.Idle;
    Direction direction = Direction.Left;
    Vector2 move = new Vector2(0, 0);

    public Pigeon(Map<Integer, Landscape> landscapes) {
        this.landscapes = landscapes;
    }

    public void setIdleAnimation(Animation animation) {idleAnimation = animation;}
    public void setFlyAnimation(Animation animation) {flyAnimation = animation;}
    public void setMoveAnimation(Animation animation) {moveAnimation = animation;}
    public void setTexture(Texture texture) {this.texture = texture;}
    public void setLandscape(Landscape landscape) {
        this.landscape = landscape;
        position.set(100, landscape.getY(100));
    }

    private void changeState(State state) {
        if (this.state != state) {
            this.state = state;
            startTime = System.nanoTime() / 1000000000.0f;
        }
    }

    public void setMove(float angle) {
        if (state == State.Idle || state == State.Moving) {
            if (angle <= 20 || angle > 290) {
                changeState(State.Moving);
                move.set(1, 0);
            } else if (angle >= 30 && angle < 160) {
                changeState(State.Flying);
                move.set(1, 0).rotate(angle);
            } else if (angle >= 150 && angle < 250) {
                changeState(State.Moving);
                move.set(-1, 0);
            } else {
                fallDown();
            }
        } else if (state == State.Flying || state == State.Falling) {
            if ((angle <= 80 || angle > 290) || (angle >= 100 && angle < 270)) {
                changeState(State.Flying);
                move.set(1, 0).rotate(angle);
            } else if (angle >= 80 && angle < 90) {
                changeState(State.Flying);
                move.set(1, 0).rotate(80);
            } else if (angle >= 90 && angle < 100) {
                changeState(State.Flying);
                move.set(1, 0).rotate(100);
            } else {
                fallDown();
            }
        }
    }

    public void drop() {
        if (state == State.Moving) {
            move.set(0, 0);
            changeState(State.Idle);
        } else if (state == State.Flying) {
            fallDown();
        }
    }

    public void fallDown() {
        if (state == State.Flying) {
            changeState(State.Falling);
            move.set(direction == Direction.Right ? 1 : -1, -5).normalize();
        }
        if (state == State.Moving || state == State.Idle) {
            position.y -= 21;
            changeState(State.Falling);
            move.set(direction == Direction.Right ? 1 : -1, -5).normalize();
        }
    }

    public void enter() {
        Door door = landscape.getDoor(position.x, position.y);
        if (door != null && landscapes.containsKey(door.toGo)) {
            landscape = landscapes.get(door.toGo);
            position = door.newCoord;
        }
    }

    public boolean isOnDoor() {
        Door door = landscape.getDoor(position.x, position.y);
        return door != null;
    }

    public void update(float deltaTime) {
        if (move.x > 0) {
            direction = Direction.Right;
        } else if (move.x < 0) {
            direction = Direction.Left;
        }
        Vector2 oldPosition = new Vector2(position);
        //changePosition
        switch (state) {
            case Falling:
                /**/
                //if (state == State.Falling)
                position.add(move.copy().mul(FALL_SPEED));
                break;
            case Flying:
                position.add(move.copy().mul(FLY_SPEED));
                break;
            case Moving:
                position.add(move.copy().mul(WALK_SPEED));
                break;
        }
        //check landscape change
        if (position.x > landscape.rightBorder) {
            if (landscape.right == null) {
                position.x = oldPosition.x;
            } else {
                landscape = landscape.right;
                position.x = landscape.leftborder + (position.x - landscape.left.rightBorder);
            }
        }
        if (position.x < landscape.leftborder) {
            if (landscape.left == null) {
                position.x = oldPosition.x;
            } else {
                landscape = landscape.left;
                position.x = landscape.rightBorder - (landscape.right.leftborder - position.x);
            }
        }
        if (position.y > landscape.roof) { //OOOOOH ITS SO HARD WITH IT BECAUSE OF GROUND LOL
            if (landscape.up == null) {
                position.y = oldPosition.y;
            } else {
                landscape = landscape.up;
            }
        }
        //check height
        int minHeight = landscape.getY(position.x);
        if (position.y < minHeight) {
            if (state == State.Flying || state == State.Falling) {
                changeState(State.Idle);
                position.set(oldPosition.x, minHeight);
                move.set(0, 0);
            }
        } else if (state == State.Falling){
            List<Surface> platforms = landscape.getPlatforms(position.x);
            if (platforms.size() > 0) {
                for (Surface a: platforms) {
                    if (position.y + 20 > a.height && position.y - 20 < a.height) {
                        changeState(State.Idle);
                        position.set(position.x, a.height);
                        move.set(0, 0);
                        break;
                    }
                }
            }
        }
        boolean notFall = false;
        if (state == State.Moving || state == State.Idle) {
            List<Surface> platforms = landscape.getPlatforms(position.x);
            for (Surface a: platforms) {
                if (position.y + 20 > a.height && position.y - 20 < a.height) {
                    position.y = a.height;
                    notFall = true;
                    break;
                }
            }
            if (!notFall) {
                fallDown();
            }
        }
    }

    public boolean isLeft() {return direction == Direction.Left;}

    public TextureRegion getTexture() {
        switch (state) {
            default:
            case Idle: return idleAnimation.getKeyFrame(System.nanoTime() / 1000000000.0f - startTime, Animation.ANIMATION_SWING);
            case Flying: return flyAnimation.getKeyFrame(System.nanoTime() / 1000000000.0f - startTime, Animation.ANIMATION_SWING);
            case Moving: return moveAnimation.getKeyFrame(System.nanoTime() / 1000000000.0f - startTime, Animation.ANIMATION_SWING);
            case Falling: return flyAnimation.getFrame(0);
        }
    }

}
