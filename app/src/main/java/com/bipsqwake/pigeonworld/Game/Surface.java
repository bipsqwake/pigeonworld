package com.bipsqwake.pigeonworld.Game;

/**
 * Created by bipsq on 20.02.2017.
 */
public class Surface {
    public int from;
    public int to;
    public int height;

    Surface(int from, int to, int height) {
        this.from = from;
        this.to = to;
        this.height = height;
    }
}
