package com.bipsqwake.pigeonworld;

import com.bipsqwake.pigeonworld.Game.Model;
import com.bipsqwake.pigeonworld.Game.StandartControl;
import com.bipsqwake.pigeonworld.framework.Screen;
import com.bipsqwake.pigeonworld.framework.gl.FileTexture;
import com.bipsqwake.pigeonworld.framework.gl.SpriteBatcher;
import com.bipsqwake.pigeonworld.framework.gl.Texture;
import com.bipsqwake.pigeonworld.framework.gl.TextureRegion;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;
import com.bipsqwake.pigeonworld.framework.impl.GLGraphics;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 18.02.2017.
 */
public class DefaultScreen extends Screen {
    TextureRegion backRegion;
    SpriteBatcher batcher;

    Model model;
    StandartControl controller;

    public DefaultScreen(GLGame game) {
        super(game);
        Texture background = new FileTexture(game, "background.png");
        background.load();
        background.bind();
        background.setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
        backRegion = new TextureRegion(background, 0, 0, 720, 1280);
        model = new Model(game);
        controller = new StandartControl(model, game);
        batcher = new SpriteBatcher(game.getGlGraphics(), 100);
    }

    @Override
    public void update(float deltaTime) {
        controller.update(deltaTime);
    }

    private void setProjectionMatrix() {
        GLGraphics graphics = ((GLGame) game).getGlGraphics();
        GL10 gl = graphics.getGL();
        float width = (1080 * ((GLGame) game).displayWidth) / ((GLGame) game).displayHeight;
        float y;
        if (model.pigeon.landscape.left == null &&  model.pigeon.position.x < width / 2) y = width / 2;
        else if (model.pigeon.landscape.right == null && model.pigeon.position.x > 1080 - width / 2) y = 1080 - width / 2;
        else y = model.pigeon.position.x;
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(y - width / 2, y + width / 2, 0, 1080, 1, -1);
    }

    @Override
    public void present(float deltaTime) {
        GLGraphics graphics = ((GLGame) game).getGlGraphics();
        GL10 gl = graphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();

        setProjectionMatrix();
        batcher.beginBatch(model.pigeon.landscape.textureRegion.texture);
        batcher.drawSprite(540, 540, 1080, 1080, model.pigeon.landscape.textureRegion, SpriteBatcher.NORMAL);
        batcher.endBatch();

        if (model.pigeon.landscape.left != null) {
            batcher.beginBatch(model.pigeon.landscape.left.textureRegion.texture);
            batcher.drawSprite(-540, 540, 1080, 1080, model.pigeon.landscape.left.textureRegion, SpriteBatcher.NORMAL);
            batcher.endBatch();
        }

        if (model.pigeon.landscape.right != null) {
            batcher.beginBatch(model.pigeon.landscape.right.textureRegion.texture);
            batcher.drawSprite(1620, 540, 1080, 1080, model.pigeon.landscape.right.textureRegion, SpriteBatcher.NORMAL);
            batcher.endBatch();
        }

        batcher.beginBatch(model.pigeon.texture);
        batcher.drawSprite(model.pigeon.position.x, model.pigeon.position.y + 50, 100, 100,
                            model.pigeon.getTexture(), model.pigeon.isLeft() ? SpriteBatcher.NORMAL : SpriteBatcher.INVERTED);
        batcher.endBatch();
        controller.present();
    }

    @Override
    public void pause() {
        model.onPause();
    }

    @Override
    public void resume() {
        GLGraphics graphics = ((GLGame) game).getGlGraphics();
        GL10 gl = graphics.getGL();
        gl.glClearColor(1,1,0,1);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glViewport(0, 0, graphics.getWidth(), graphics.getHeight());
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        setProjectionMatrix();
        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        model.reload();
        controller.reload();
    }

    @Override
    public void dispose() {

    }
}
