package com.bipsqwake.pigeonworld.gui;

import android.graphics.Typeface;

import com.bipsqwake.pigeonworld.framework.Input;
import com.bipsqwake.pigeonworld.framework.Screen;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;

import java.util.LinkedList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 17.03.2017.
 */
public abstract class Menu {
    List<BackgroundButton> list = new LinkedList<>();
    GLGame game;
    Screen screen;
    int itemWidth;
    int itemHeight;
    String font;
    String background;

    public Menu(GLGame game, Screen screen) {
        this.game = game;
        this.screen = screen;
    }

    public void add(String text) {
        BackgroundButton button = new BackgroundButton(game, background, text, font, itemWidth, itemHeight);
        button.setId(list.size());
        list.add(button);
    }

    public void commit() {
        int shift = 1000 / list.size();
        int start = shift / 2;
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setPosition(360, 1000 - start - shift * i);
        }
    }

    public void setItemParams(int width, int height, String font, String background) {
        this.itemHeight = height;
        this.itemWidth = width;
        this.font = font;
        this.background = background;
    }

    public void present() {
        GL10 gl = game.getGlGraphics().getGL();
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(0, 720, 0, 1280, 1, -1);
        for (BackgroundButton a: list) {
            a.draw(game);
        }
    }

    public void update() {
        List<Input.TouchEvent> events = game.getInput().getTouchEvents();
        for (int i = 0; i < events.size(); i++) {
            Input.TouchEvent event = events.get(i);
            for (BackgroundButton a: list) {
                if (a.isInBounds(event.x, event.y)) {
                    perform(a.id);
                }
            }
        }
    }

    public abstract void perform(int id);
}
