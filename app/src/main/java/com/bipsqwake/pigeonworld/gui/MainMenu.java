package com.bipsqwake.pigeonworld.gui;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bipsqwake.pigeonworld.Database.DBHelper;
import com.bipsqwake.pigeonworld.DefaultScreen;
import com.bipsqwake.pigeonworld.framework.Screen;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;

/**
 * Created by bipsq on 17.03.2017.
 */
public class MainMenu extends Menu {
    public MainMenu(GLGame game, Screen screen) {
        super(game, screen);
        setItemParams(400, 128,"standart.ttf", "brownButton.png");
        add("New Game");
        add("Continue");
        add("Options");
        commit();
    }

    @Override
    public void perform(int id) {
        switch (id) {
            case 0:
                DBHelper helper = new DBHelper(game, game.getFileIO());
                SQLiteDatabase db = helper.getWritableDatabase();
                db.delete("savedposition", null, null);
                ContentValues cv = new ContentValues();
                cv.put("x", 100);
                cv.put("y", 113); //TODO autofall
                cv.put("land", 1);
                db.insert("savedposition", null, cv);
                game.setScreen(new DefaultScreen(game));
                break;
            case 1:
                game.setScreen(new DefaultScreen(game));
                break;
            case 2:
                break;
        }
    }
}
