package com.bipsqwake.pigeonworld.gui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import com.bipsqwake.pigeonworld.framework.FileIO;
import com.bipsqwake.pigeonworld.framework.gl.BitmapTexture;
import com.bipsqwake.pigeonworld.framework.gl.FileTexture;
import com.bipsqwake.pigeonworld.framework.gl.SpriteBatcher;
import com.bipsqwake.pigeonworld.framework.gl.Texture;
import com.bipsqwake.pigeonworld.framework.gl.TextureRegion;
import com.bipsqwake.pigeonworld.framework.gl.Vertices;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;

import java.io.IOException;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 17.03.2017.
 */
public class BackgroundButton {
    Texture texture;
    String text;
    Typeface font;
    TextureRegion region;
    Vertices vertices;
    int x;
    int y;
    int width;
    int height;
    int id;

    public BackgroundButton(GLGame game, String pathToBackground, String text, String pathToFont, int width, int height) {
        this.text = text;
        this.width = width;
        this.height = height;
        FileIO fileIO = game.getFileIO();
        font = Typeface.createFromAsset(fileIO.getAssets(), pathToFont);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(fileIO.readAsset(pathToBackground));
            Bitmap change = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(change);
            Paint paint = new Paint();
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTypeface(font);
            paint.setTextSize(30);
            canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), new Rect(0, 0, width, height), null);
            canvas.drawText(text, width / 2, height / 2, paint);
            bitmap = change;
        } catch (IOException e) {
            e.printStackTrace();
        }
        texture = new BitmapTexture(game, bitmap);
        texture.load();
        texture.bind();
        texture.setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
        region = new TextureRegion(texture, 0, 0, 256, 128);
        vertices = new Vertices(game.getGlGraphics(), false, true, 4, 6);
        vertices.setVertices(new float[] {width / 2, height / 2, 1, 0,
                -width / 2, height / 2, 0, 0,
                -width / 2, -height / 2, 0, 1,
                width / 2, -height / 2, 1, 1}, 0, 16);
        vertices.setIndices(new short[] {0, 1, 2, 2, 3, 0}, 0, 6);
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void draw(GLGame game) {
        GL10 gl = game.getGlGraphics().getGL();
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslatef(x, y, 0);
        texture.bind();
        vertices.bind();
        vertices.draw(GL10.GL_TRIANGLES, 0, 6);
        vertices.unbind();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean isInBounds(int x, int y) {
        return (x > this.x - width / 2 && x < this.x + width / 2) && (y > this.y - height / 2 && y < this.y + height / 2);
    }
}
