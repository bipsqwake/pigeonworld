package com.bipsqwake.pigeonworld.framework.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.util.Scanner;

/**
 * Created by bipsq on 17.03.2017.
 */
public class Utils {
    public static String fromStream(InputStream in) {
        /*Scanner s = new Scanner(in).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";*/
        StringBuilder result = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
