package com.bipsqwake.pigeonworld.framework;

/**
 * Created by 02.02.2017 on 02.02.2017.
 */
public interface Audio {
    public Music newMusic(String filename);
    public Sound newSound(String filename);
}
