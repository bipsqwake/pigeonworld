package com.bipsqwake.pigeonworld.framework.gl;

import com.bipsqwake.pigeonworld.framework.math.Vector2;

/**
 * Created by bipsq on 17.03.2017.
 */
public class SingleAnimationObject {
    Animation animation;
    float startTime;
    Vector2 position;
    Vector2 direction;
    float speed;
    boolean right;

    public SingleAnimationObject(Animation animation) {
        this.animation = animation;
        startTime = System.nanoTime() / 1000000000.0f;
    }

    public TextureRegion getFrame() {
        return animation.getKeyFrame(System.nanoTime() / 1000000000.0f - startTime, Animation.ANIMATION_SWING);
    }

    public void setPosition(int x, int y) {
        position = new Vector2(x, y);
    }

    public void setDirection(Vector2 direction) {
        this.direction = new Vector2(direction);
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setRight (boolean right) {
        this.right = right;
    }

    public Vector2 getPosition() {
        return position;
    }

    public boolean isRight() {
        return right;
    }

    public void update() {
        position.add(direction.copy().mul(speed));
    }
}
