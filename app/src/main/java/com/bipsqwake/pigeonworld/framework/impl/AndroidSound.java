package com.bipsqwake.pigeonworld.framework.impl;

import android.media.SoundPool;

import com.bipsqwake.pigeonworld.framework.Sound;

/**
 * Created by 03.02.2017 on 03.02.2017.
 */
public class AndroidSound implements Sound {
    SoundPool soundPool;
    int soundID;
    public AndroidSound(SoundPool soundPool, int soundID) {
        this.soundID = soundID;
        this.soundPool = soundPool;
    }

    @Override
    public void play(float volume) {
        soundPool.play(soundID, volume, volume, 0, 0, 1);
    }

    @Override
    public void dispose() {
        soundPool.unload(soundID);
    }
}
