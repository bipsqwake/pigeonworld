package com.bipsqwake.pigeonworld.framework;

/**
 * Created by 02.02.2017 on 02.02.2017.
 */
public interface Music {
    public void play();
    public void stop();
    public void pause();
    public void setLooping(boolean looping);
    public void setVolume(float volume);
    public boolean isPlaying();
    public boolean isStopped();
    public boolean isLooped();
    public void dispose();
}
