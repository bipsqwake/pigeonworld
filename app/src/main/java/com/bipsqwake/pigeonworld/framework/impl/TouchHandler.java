package com.bipsqwake.pigeonworld.framework.impl;

import android.view.View;

import com.bipsqwake.pigeonworld.framework.Input;
import com.bipsqwake.pigeonworld.framework.Input.TouchEvent;

import java.util.List;

/**
 * Created by 04.02.2017 on 04.02.2017.
 */
public interface TouchHandler extends View.OnTouchListener {
    public boolean isTouchDown(int pointer);
    public int getTouchX(int pointer);
    public int getTouchY(int pointer);
    public List<TouchEvent> getTouchEvents();
}
