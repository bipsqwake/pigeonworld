package com.bipsqwake.pigeonworld.framework.impl;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.bipsqwake.pigeonworld.framework.Audio;
import com.bipsqwake.pigeonworld.framework.Music;
import com.bipsqwake.pigeonworld.framework.Sound;

import java.io.IOException;

/**
 * Created by 03.02.2017 on 03.02.2017.
 */
public class AndroidAudio implements Audio {
    AssetManager assets;
    SoundPool soundPool;
    public AndroidAudio(Activity activity) {
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
    }

    @Override
    public Music newMusic(String filename) {
        try {
            AssetFileDescriptor desc = assets.openFd(filename);
            return new AndroidMusic(desc);
        } catch (IOException e) {
            throw new RuntimeException("Cant load music " + filename);
        }
    }

    @Override
    public Sound newSound(String filename) {
        try {
            AssetFileDescriptor desc = assets.openFd(filename);
            int soundID = soundPool.load(desc, 0);
            return new AndroidSound(soundPool, soundID);
        } catch (IOException e) {
            throw new RuntimeException("Cant load sound " + filename);
        }
    }
}
