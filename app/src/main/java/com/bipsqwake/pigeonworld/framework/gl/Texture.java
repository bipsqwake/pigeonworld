package com.bipsqwake.pigeonworld.framework.gl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.Log;

import com.bipsqwake.pigeonworld.framework.FileIO;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;
import com.bipsqwake.pigeonworld.framework.impl.GLGraphics;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 13.02.2017.
 */
public abstract class Texture {
    GLGraphics graphics;
    float width;
    float height;
    int textureID;
    int magFilter;
    int minFilter;

    public Texture(GLGame game) {
        this.graphics = game.getGlGraphics();
    }

    public abstract void load();

    public void setFilters(int minFilter, int magFilter) {
        GL10 gl = graphics.getGL();
        this.minFilter = minFilter;
        this.magFilter = magFilter;
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, this.magFilter);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, this.minFilter);
    }

    public void bind() {
        GL10 gl = graphics.getGL();
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
    }

    public void reload() {
        load();
        bind();
        setFilters(minFilter, magFilter);
        graphics.getGL().glBindTexture(GL10.GL_TEXTURE_2D, textureID);
    }

    public void dispose() {
        int textures[] = {textureID};
        GL10 gl = graphics.getGL();
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
        gl.glDeleteTextures(1, textures, 0);
    }

    public void standartLoad() {
        load();
        bind();
        setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
    }
}
