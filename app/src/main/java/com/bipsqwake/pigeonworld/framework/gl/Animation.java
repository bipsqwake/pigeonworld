package com.bipsqwake.pigeonworld.framework.gl;

/**
 * Created by bipsq on 18.02.2017.
 */
public class Animation {
    public static final int ANIMATION_LOOPING = 0;
    public static final int ANIMATION_NONLOOPING = 1;
    public static final int ANIMATION_SWING = 2;
    final TextureRegion[] keyFrames;
    final float frameDuration;

    public Animation(float frameDuration, TextureRegion ... keyFrames) {
        this.frameDuration = frameDuration;
        this.keyFrames = keyFrames;
    }

    public TextureRegion getKeyFrame(float stateTime, int mode) {
        int frameNumber = (int) (stateTime / frameDuration);
        if (mode == ANIMATION_NONLOOPING) {
            frameNumber = Math.min(frameNumber, keyFrames.length - 1);
        } else if (mode == ANIMATION_SWING) {
            frameNumber = frameNumber % (keyFrames.length * 2 - 2);
            frameNumber = frameNumber < keyFrames.length ? frameNumber : 2 * keyFrames.length - frameNumber - 2;
        } else {
            frameNumber = frameNumber % keyFrames.length;
        }
        return keyFrames[frameNumber];
    }

    public TextureRegion getFrame(int num) {
        return keyFrames[num];
    }
}