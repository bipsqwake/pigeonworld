package com.bipsqwake.pigeonworld.framework;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 04.02.2017 on 04.02.2017.
 */
public class Pool<T> {
    public interface PoolObjectFactory<T> {
        public T createObject();
    }
    private final List<T> freeObjects;
    private final PoolObjectFactory<T> factory;
    private final int maxSize;

    public Pool(PoolObjectFactory<T> factory, int maxSize) {
        this.factory = factory;
        this.maxSize = maxSize;
        this.freeObjects = new ArrayList<>(maxSize);
    }

    public T newObject() {
        T result = null;
        if (freeObjects.size() == 0) {
            result = factory.createObject();
        } else {
            result = freeObjects.remove(freeObjects.size() - 1);
        }
        return result;
    }

    public void free(T object) {
        if (freeObjects.size() < maxSize) {
            freeObjects.add(object);
        }
    }
}
