package com.bipsqwake.pigeonworld.framework.impl;

import android.graphics.Bitmap;

import com.bipsqwake.pigeonworld.framework.Graphics;
import com.bipsqwake.pigeonworld.framework.Pixmap;

/**
 * Created by 05.02.2017 on 05.02.2017.
 */
public class AndroidPixmap implements Pixmap {
    Bitmap bitmap;
    Graphics.PixmapFormat pixmapFormat;

    public AndroidPixmap (Bitmap bitmap, Graphics.PixmapFormat pixmapFormat) {
        this.bitmap = bitmap;
        this.pixmapFormat = pixmapFormat;
    }

    @Override
    public int getWidth() {
        return bitmap.getWidth();
    }

    @Override
    public int getHeight() {
        return bitmap.getHeight();
    }

    @Override
    public Graphics.PixmapFormat getFormat() {
        return pixmapFormat;
    }

    @Override
    public void dispose() {
        bitmap.recycle();
    }
}
