package com.bipsqwake.pigeonworld.framework;

/**
 * Created by 02.02.2017 on 02.02.2017.
 */
public abstract class Screen {
    protected final Game game;
    public Screen(Game game) {
        this.game = game;
    }
    public abstract void update(float deltaTime);
    public abstract void present(float deltaTime);
    public abstract void pause();
    public abstract void resume();
    public abstract void dispose();
}
