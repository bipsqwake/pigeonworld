package com.bipsqwake.pigeonworld.framework;

/**
 * Created by 02.02.2017 on 02.02.2017.
 */
public interface Game {
    public Input getInput();
    public FileIO getFileIO();
    public Graphics getGraphics();
    public Audio getAudio();
    public void setScreen(Screen screen);
    public Screen getCurrentScreen();
    public Screen getStartScreen();
}
