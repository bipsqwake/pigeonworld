package com.bipsqwake.pigeonworld.framework.math;

import android.util.FloatMath;

/**
 * Created by bipsq on 18.02.2017.
 */
public class  Vector2 {
    public static float TO_RADIANS = (1 / 180.0f) * (float) Math.PI;
    public static float TO_DEGREES = (1 / (float) Math.PI) * 180;
    public float x, y;

    public Vector2() {}

    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2 (Vector2 vector2) {
        this.x = vector2.x;
        this.y = vector2.y;
    }

    public Vector2 copy() {
        return new Vector2(this);
    }

    public Vector2 set(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Vector2 set(Vector2 vector2) {
        this.x = vector2.x;
        this.y = vector2.y;
        return this;
    }

    public Vector2 add(float x, float y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public Vector2 add(Vector2 vector2) {
        this.x += vector2.x;
        this.y += vector2.y;
        return this;
    }

    public Vector2 sub(float x, float y) {
        this.x -= x;
        this.y -= y;
        return this;
    }

    public Vector2 sub(Vector2 vector2) {
        this.x -= vector2.x;
        this.y -= vector2.y;
        return this;
    }

    public Vector2 mul(float a) {
        this.x *= a;
        this.y *= a;
        return this;
    }

    public float len() {
        return (float)Math.sqrt(x * x + y * y);
    }

    public Vector2 normalize() {
        float len = len();
        if (len != 0) {
            x /= len;
            y /= len;
        }
        return this;
    }

    public float angle() {
        float angle = (float)Math.atan2(y, x) * TO_DEGREES;
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }

    public Vector2 rotate(float angle) {
        float radAngle = angle * TO_RADIANS;
        float cos = (float)Math.cos(radAngle);
        float sin = (float)Math.sin(radAngle);
        float newX = this.x * cos - this.y * sin;
        float newY = this.x * sin + this.y * cos;
        this.x = newX;
        this.y = newY;
        return this;
    }

    public float dist(float x, float y) {
        float distX = this.x - x;
        float distY = this.y - y;
        return (float)Math.sqrt(distX * distX + distY * distY);
    }

    public float dist(Vector2 vector2) {
        float distX = this.x - vector2.x;
        float distY = this.y - vector2.y;
        return (float)Math.sqrt(distX * distX + distY * distY);
    }

    public static float angle(Vector2 first, Vector2 second) {
        float an1 = (float)Math.atan2(first.y, first.x);
        float an2 = (float)Math.atan2(second.y, second.x);
        if (an1 < 0) an1 += 2 * Math.PI;
        if (an2 < 0) an2 += 2 * Math.PI;
        return (an2 - an1) * TO_DEGREES;
    }
}

