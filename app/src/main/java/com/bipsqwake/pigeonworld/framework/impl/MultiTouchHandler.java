package com.bipsqwake.pigeonworld.framework.impl;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.bipsqwake.pigeonworld.framework.Input;
import com.bipsqwake.pigeonworld.framework.Pool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 04.02.2017 on 04.02.2017.
 */
public class MultiTouchHandler implements TouchHandler {
    boolean[] isTouched = new boolean[20];
    int[] touchX = new int[20];
    int[] touchY = new int[20];
    Pool<Input.TouchEvent> touchEventPool;
    List<Input.TouchEvent> touchEvents = new ArrayList<>();
    List<Input.TouchEvent> touchEventBuffer = new ArrayList<>();
    float scaleX;
    float scaleY;

    public MultiTouchHandler (View view, float scaleX, float scaleY) {
        Pool.PoolObjectFactory<Input.TouchEvent> factory = new Pool.PoolObjectFactory<Input.TouchEvent>() {
            @Override
            public Input.TouchEvent createObject() {
                return new Input.TouchEvent();
            }
        };
        view.setOnTouchListener(this);
        touchEventPool = new Pool<>(factory, 100);
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }
    @Override
    public boolean isTouchDown(int pointer) {
        synchronized (this){
            if (pointer < 0 || pointer >= 20) {
                return false;
            } else {
                return isTouched[pointer];
            }
        }
    }

    @Override
    public int getTouchX(int pointer) {
        synchronized (this) {
            if (pointer < 0 || pointer >= 20) {
                return 0;
            } else {
                return touchX[pointer];
            }
        }
    }

    @Override
    public int getTouchY(int pointer) {
        synchronized (this) {
            if (pointer < 0 || pointer >= 20) {
                return 0;
            } else {
                return touchY[pointer];
            }
        }
    }

    @Override
    public List<Input.TouchEvent> getTouchEvents() {
        synchronized (this) {
            int len = touchEvents.size();
            for (int i = 0; i < len; i++) {
                touchEventPool.free(touchEvents.get(i));
            }
            touchEvents.clear();
            touchEvents.addAll(touchEventBuffer);
            touchEventBuffer.clear();
            return touchEvents;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        synchronized (this) {
            int action = event.getAction() & MotionEvent.ACTION_MASK;
            int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
            int pointerID = event.getPointerId(pointerIndex);
            Input.TouchEvent touchEvent;
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    touchEvent = touchEventPool.newObject();
                    touchEvent.type = Input.TouchEvent.TOUCH_DOWN;
                    touchEvent.x = touchX[pointerID] = (int)(event.getX() * scaleX);
                    touchEvent.y = touchY[pointerID] = (int)(1280 - (event.getY()) * scaleY);
                    touchEvent.pointer = pointerID;
                    isTouched[pointerID] = true;
                    touchEventBuffer.add(touchEvent);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    touchEvent = touchEventPool.newObject();
                    touchEvent.type = Input.TouchEvent.TOUCH_UP;
                    touchEvent.x = touchX[pointerID] = (int)(event.getX() * scaleX);
                    touchEvent.y = touchY[pointerID] = (int)(1280 - (event.getY()) * scaleY);
                    touchEvent.pointer = pointerID;
                    isTouched[pointerID] = false;
                    touchEventBuffer.add(touchEvent);
                    break;
                case MotionEvent.ACTION_MOVE:
                    int pointerCount = event.getPointerCount();
                    for (int i = 0; i < pointerCount; i++) {
                        pointerIndex = i;
                        pointerID = event.getPointerId(pointerIndex);
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = Input.TouchEvent.TOUCH_DRAGGED;
                        touchEvent.pointer = pointerID;
                        touchEvent.x = touchX[pointerID] = (int)(event.getX(pointerIndex) * scaleX);
                        touchEvent.y = touchY[pointerID] = (int)(1280 - (event.getY(pointerIndex)) * scaleY);
                        touchEventBuffer.add(touchEvent);
                    }
                    break;
            }
            return true;
        }
    }

    int getRealX(int innerX) {
        return (int) (innerX / scaleX);
    }

    int getRealY(int innerY) {
        return (int) (innerY / scaleY);
    }
}
