package com.bipsqwake.pigeonworld.framework.gl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.Log;

import com.bipsqwake.pigeonworld.framework.FileIO;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 17.03.2017.
 */
public class FileTexture extends Texture {
    private FileIO fileIO;
    String filename;

    public FileTexture(GLGame game, String filename) {
        super(game);
        this.fileIO = game.getFileIO();
        this.filename = filename;
    }

    public void load() {
        GL10 gl = graphics.getGL();
        int textureIDs[] = new int[1];
        gl.glGenTextures(1, textureIDs, 0);
        textureID = textureIDs[0];
        InputStream in = null;
        try {
            in = fileIO.readAsset(filename);
            Bitmap bitmap = BitmapFactory.decodeStream(in);
            this.width = bitmap.getWidth();
            this.height = bitmap.getHeight();
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
            gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);

        } catch (IOException e) {
            throw new RuntimeException("Cant't load file " + filename + e + " ");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}