package com.bipsqwake.pigeonworld.framework;

import android.content.res.AssetManager;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 02.02.2017 on 02.02.2017.
 */
public interface FileIO {
    public InputStream readAsset(String filename) throws IOException;
    public FileOutputStream readFile(String filename) throws IOException;
    public FileInputStream writeFile(String filename) throws IOException;
    public AssetManager getAssets();
}
