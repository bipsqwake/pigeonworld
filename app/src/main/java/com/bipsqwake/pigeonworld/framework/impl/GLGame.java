package com.bipsqwake.pigeonworld.framework.impl;

import android.app.Activity;
import android.app.admin.SystemUpdatePolicy;
import android.content.Intent;
import android.content.res.ObbInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.bipsqwake.pigeonworld.framework.Audio;
import com.bipsqwake.pigeonworld.framework.FileIO;
import com.bipsqwake.pigeonworld.framework.Game;
import com.bipsqwake.pigeonworld.framework.Graphics;
import com.bipsqwake.pigeonworld.framework.Input;
import com.bipsqwake.pigeonworld.framework.Screen;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 11.02.2017.
 */
public abstract class GLGame extends Activity implements Game, GLSurfaceView.Renderer {
    enum GLGameState {
        Initialized,
        Running,
        Paused,
        Finished,
        Idle
    }

    GLSurfaceView glView;
    GLGraphics glGraphics;
    Audio audio;
    Input input;
    FileIO fileIO;
    Screen screen;
    GLGameState gameState = GLGameState.Initialized;
    Object stateChanged = new Object();
    long startTime = System.nanoTime();

    public float displayWidth;
    public float displayHeight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        displayWidth = metrics.widthPixels;
        displayHeight = metrics.heightPixels;
        glView = new GLSurfaceView(this);
        glView.setRenderer(this);
        setContentView(glView);
        glGraphics = new GLGraphics(glView);
        audio = new AndroidAudio(this);
        float scaleX = (float) 720 / metrics.widthPixels;
        float scaleY = (float) 1280 / metrics.heightPixels;
        input = new AndroidInput(glView, this, scaleX, scaleY);
        fileIO = new AndroidFileIO(getAssets(), this);
    }

    public void onResume() {
        super.onResume();
        glView.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        glGraphics.setGL(gl);
        synchronized (stateChanged) {
            if (gameState == GLGameState.Initialized) {
                screen = getStartScreen();
            }
            gameState = GLGameState.Running;
            screen.resume();
            startTime = System.nanoTime();
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
    }

    public void onDrawFrame(GL10 gl) {
        GLGameState gameState;
        synchronized (stateChanged) {
            gameState = this.gameState;
        }
        if (gameState == GLGameState.Running) {
            float deltaTime = (startTime - System.nanoTime()) / 1000000000.0f;
            startTime = System.nanoTime();
            screen.update(deltaTime);
            screen.present(deltaTime);
        }

        if (gameState == GLGameState.Paused) {
            screen.pause();
            synchronized (stateChanged) {
                this.gameState = GLGameState.Idle;
                stateChanged.notifyAll();
            }
        }
        if (gameState == GLGameState.Finished) {
            screen.pause();
            screen.dispose();
            synchronized (stateChanged) {
                this.gameState = GLGameState.Idle;
                stateChanged.notifyAll();
            }
        }
    }

    @Override
    public void onPause() {
        synchronized (stateChanged) {
            if (isFinishing()) {
                gameState = GLGameState.Finished;
            } else {
                gameState = GLGameState.Paused;
            }
            while (true) {
                try {
                    stateChanged.wait();
                    break;
                } catch (InterruptedException e) {

                }
            }
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            glView.onPause();
            super.onPause();
        }
    }

    public GLGraphics getGlGraphics() {
        return glGraphics;
    }

    @Override
    public Input getInput() {
        return input;
    }

    @Override
    public Audio getAudio() {
        return audio;
    }

    @Override
    public FileIO getFileIO() {
        return fileIO;
    }

    @Override
    public Graphics getGraphics() {
        throw new IllegalStateException("OPENGL");
    }

    @Override
    public void setScreen (Screen screen) {
        if (screen == null) {
            throw new IllegalArgumentException("Screen can't be null");
        }
        this.screen.pause();
        this.screen.dispose();
        screen.resume();
        screen.update(0);
        this.screen = screen;
    }

    @Override
    public Screen getCurrentScreen() {
        return screen;
    }
}
