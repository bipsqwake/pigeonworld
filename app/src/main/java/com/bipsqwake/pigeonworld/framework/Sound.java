package com.bipsqwake.pigeonworld.framework;

/**
 * Created by 02.02.2017 on 02.02.2017.
 */
public interface Sound {
    public void play(float volume);
    public void dispose();
}
