package com.bipsqwake.pigeonworld.framework.gl;

import com.bipsqwake.pigeonworld.framework.impl.GLGraphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 13.02.2017.
 */
public class Vertices {
    final GLGraphics graphics;
    final boolean hasColor;
    final boolean hasTexCoords;
    final int vertexSize;
    final FloatBuffer vertices;
    final ShortBuffer indices;
    //int[] tmpBuffer;


    public Vertices(GLGraphics graphics, boolean hasColor, boolean hasTexCoords, int maxVertices, int maxIndices) {
        this.graphics = graphics;
        this.hasColor = hasColor;
        this.hasTexCoords = hasTexCoords;
        this.vertexSize = (2 + (hasColor?4:0) + (hasTexCoords?2:0)) * 4;
        //this.tmpBuffer = new int[maxVertices * vertexSize * 4];
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertexSize * maxVertices);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertices = byteBuffer.asFloatBuffer();
        if (maxIndices > 0) {
            byteBuffer = ByteBuffer.allocateDirect(maxIndices * Short.SIZE / 8);
            byteBuffer.order(ByteOrder.nativeOrder());
            indices = byteBuffer.asShortBuffer();
        } else {
            indices = null;
        }
    }

    public void setVertices(float[] vertices, int offset, int length) {
        this.vertices.clear();
        //int len = offset + length;
        //for (int i = offset, j = 0; i < len; i++, j++) {
        //    tmpBuffer[j] = Float.floatToRawIntBits(vertices[i]);
        //}
        this.vertices.put(vertices, offset, length);
        this.vertices.flip();
    }

    public void setIndices(short[] indices, int offset, int length) {
        this.indices.clear();
        this.indices.put(indices, offset, length);
        this.indices.flip();
    }

    public void bind() {
        GL10 gl = graphics.getGL();
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        vertices.position(0);
        gl.glVertexPointer(2, GL10.GL_FLOAT, vertexSize, vertices);
        if(hasColor) {
            gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
            vertices.position(2);
            gl.glColorPointer(4, GL10.GL_FLOAT, vertexSize, vertices);
        }
        if(hasTexCoords) {
            gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
            vertices.position(hasColor?6:2);
            gl.glTexCoordPointer(2, GL10.GL_FLOAT, vertexSize, vertices);
        }
    }

    public void draw(int type, int offset, int num) {
        GL10 gl = graphics.getGL();

        if(indices!=null) {
            indices.position(offset);
            gl.glDrawElements(type, num,
                    GL10.GL_UNSIGNED_SHORT, indices);
        } else {
            gl.glDrawArrays(type, offset, num);
        }
    }

    public void unbind() {
        GL10 gl = graphics.getGL();
        if(hasTexCoords)
            gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        if(hasColor)
            gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
    }
}
