package com.bipsqwake.pigeonworld.framework.impl;

import android.content.Context;
import android.content.res.AssetManager;

import com.bipsqwake.pigeonworld.framework.FileIO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 03.02.2017 on 03.02.2017.
 */
public class AndroidFileIO implements FileIO {
    AssetManager assets;
    Context context;
    public AndroidFileIO(AssetManager assets, Context context) {
        this.assets = assets;
        this.context = context;
    }

    public InputStream readAsset(String filename) throws IOException{
        return assets.open(filename);
    }

    public FileOutputStream readFile(String filename) throws IOException {
        return context.openFileOutput(filename, Context.MODE_PRIVATE);
    }

    public FileInputStream writeFile(String filename) throws  IOException {
        return context.openFileInput(filename);
    }

    @Override
    public AssetManager getAssets() {
        return assets;
    }
}
