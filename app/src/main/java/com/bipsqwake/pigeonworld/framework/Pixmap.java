package com.bipsqwake.pigeonworld.framework;

import com.bipsqwake.pigeonworld.framework.Graphics.PixmapFormat;

/**
 * Created by 02.02.2017 on 02.02.2017.
 */
public interface Pixmap {
    public int getWidth();
    public int getHeight();
    public PixmapFormat getFormat();
    public void dispose();
}
