package com.bipsqwake.pigeonworld.framework.impl;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import com.bipsqwake.pigeonworld.framework.Music;

import java.io.IOException;

/**
 * Created by 03.02.2017 on 03.02.2017.
 */
public class AndroidMusic implements Music, MediaPlayer.OnCompletionListener {
    MediaPlayer player;
    boolean isPrepared = false;

    public AndroidMusic(AssetFileDescriptor descriptor) {
        player = new MediaPlayer();
        try {
            player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            player.prepare();
            player.setOnCompletionListener(this);
        } catch (IOException e) {
            throw new RuntimeException("Can't create music");
        }
    }

    @Override
    public void dispose() {
        if (player.isPlaying()) {
            player.stop();
        }
        player.release();
    }

    @Override
    public boolean isLooped() {
        return player.isLooping();
    }

    @Override
    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    public boolean isStopped() {
        return !isPrepared;
    }

    @Override
    public void play() {
        if (player.isPlaying()) {
            return;
        }
        try {
            synchronized (this) {
                if (!isPrepared)
                    player.prepare(); //maybe isPrepared = true?
                player.start();
            }
        } catch (IOException e) {
            throw new RuntimeException("Can't play music");
        }
    }

    @Override
    public void setLooping(boolean isLooping) {
        player.setLooping(isLooping);
    }

    @Override
    public void setVolume(float volume) {
        player.setVolume(volume, volume);
    }

    @Override
    public void stop() {
        player.stop();
        synchronized (this) {
            isPrepared = false;
        }
    }

    @Override
    public void pause() {
        player.pause(); //mb sth wrong
    }

    @Override
    public void onCompletion(MediaPlayer player) {
        synchronized (this) {
            isPrepared = false;
        }
    }
}
