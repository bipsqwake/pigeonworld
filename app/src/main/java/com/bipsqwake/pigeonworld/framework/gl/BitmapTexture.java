package com.bipsqwake.pigeonworld.framework.gl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import com.bipsqwake.pigeonworld.framework.impl.GLGame;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 17.03.2017.
 */
public class BitmapTexture extends Texture {
    Bitmap bitmap;

    public BitmapTexture(GLGame game, Bitmap bitmap) {
        super(game);
        this.bitmap = bitmap;
    }

    @Override
    public void load() {
        GL10 gl = graphics.getGL();
        int textureIDs[] = new int[1];
        gl.glGenTextures(1, textureIDs, 0);
        textureID = textureIDs[0];
        this.width = bitmap.getWidth();
        this.height = bitmap.getHeight();
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
    }
}
