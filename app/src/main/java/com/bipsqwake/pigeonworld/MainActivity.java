package com.bipsqwake.pigeonworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bipsqwake.pigeonworld.framework.Screen;

public class MainActivity extends com.bipsqwake.pigeonworld.framework.impl.GLGame {

    @Override
    public Screen getStartScreen() {
        return new MainMenuScreen(this);
    }
}
