package com.bipsqwake.pigeonworld;

import android.util.Log;

import com.bipsqwake.pigeonworld.framework.Game;
import com.bipsqwake.pigeonworld.framework.Screen;
import com.bipsqwake.pigeonworld.framework.gl.Animation;
import com.bipsqwake.pigeonworld.framework.gl.FileTexture;
import com.bipsqwake.pigeonworld.framework.gl.SingleAnimationObject;
import com.bipsqwake.pigeonworld.framework.gl.SpriteBatcher;
import com.bipsqwake.pigeonworld.framework.gl.Texture;
import com.bipsqwake.pigeonworld.framework.gl.TextureRegion;
import com.bipsqwake.pigeonworld.framework.impl.GLGame;
import com.bipsqwake.pigeonworld.framework.impl.GLGraphics;
import com.bipsqwake.pigeonworld.framework.math.Vector2;
import com.bipsqwake.pigeonworld.gui.BackgroundButton;
import com.bipsqwake.pigeonworld.gui.MainMenu;
import com.bipsqwake.pigeonworld.gui.Menu;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by bipsq on 17.03.2017.
 */
public class MainMenuScreen extends Screen {
    Menu mainMenu;
    TextureRegion background;
    SpriteBatcher batcher;
    List<SingleAnimationObject> pigeons = new LinkedList<>();
    Animation pigeonAnim;
    Texture pigeonAtlas;
    float startTime;

    public MainMenuScreen(GLGame game) {
        super(game);
        mainMenu = new MainMenu(game, this);
        Texture texture = new FileTexture(game, "house.png");
        texture.standartLoad();
        background = new TextureRegion(texture, 0, 0, 1080, 1080);
        batcher = new SpriteBatcher(game.getGlGraphics(), 30);
        pigeonAtlas = new FileTexture(game, "pigeon.png");
        pigeonAtlas.standartLoad();
        pigeonAnim = new Animation(0.1f, new TextureRegion(pigeonAtlas, 0, 128, 128, 128),
                new TextureRegion(pigeonAtlas, 128, 128, 128, 128),
                new TextureRegion(pigeonAtlas, 256, 128, 128, 128),
                new TextureRegion(pigeonAtlas, 384, 128, 128, 128));
        startTime = System.nanoTime() / 1000000000.0f;
    }

    private void throwPigeon() {
        Random random = new Random();
        SingleAnimationObject newPigeon = new SingleAnimationObject(pigeonAnim);
        boolean right = random.nextBoolean();
        newPigeon.setPosition(right ? 1 : 1279, random.nextInt(800) + 200);
        newPigeon.setDirection(new Vector2(right ? 1 : -1, 0).rotate(random.nextInt(90) - 45));
        newPigeon.setSpeed(random.nextFloat() * 4 + 2);
        newPigeon.setRight(right);
        pigeons.add(newPigeon);
    }

    @Override
    public void update(float deltaTime) {
        for(SingleAnimationObject a: pigeons) {
            a.update();
        }
        Iterator<SingleAnimationObject> iterator = pigeons.iterator();
        while (iterator.hasNext()) {
            SingleAnimationObject item = iterator.next();
            if (item.getPosition().x < 0 || item.getPosition().x > 1280)
                iterator.remove();
        }
        if (System.nanoTime() / 1000000000.0f > startTime) {
            throwPigeon();
            //Log.d("PIGEON", "THROW");
            Random random = new Random();
            startTime = System.nanoTime() / 1000000000.0f + random.nextFloat() * 2;
        }
        mainMenu.update();
    }

    @Override
    public void present(float deltaTime) {
        GLGraphics graphics = ((GLGame) game).getGlGraphics();
        GL10 gl = graphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(0, 720, 0, 1280, 1, -1);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        batcher.beginBatch(background.texture);
        batcher.drawSprite(360, 640, 1280, 1280, background, SpriteBatcher.NORMAL);
        batcher.endBatch();
        if (!pigeons.isEmpty()) {
            batcher.beginBatch(pigeonAtlas);
            for (SingleAnimationObject a: pigeons) {
                batcher.drawSprite(a.getPosition().x, a.getPosition().y, 100, 100, a.getFrame(), a.isRight() ? SpriteBatcher.INVERTED : SpriteBatcher.NORMAL);
            }
            batcher.endBatch();
        }
        mainMenu.present();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        GLGraphics graphics = ((GLGame) game).getGlGraphics();
        GL10 gl = graphics.getGL();
        gl.glClearColor(1,1,0,1);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glViewport(0, 0, graphics.getWidth(), graphics.getHeight());
        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public void dispose() {

    }
}
